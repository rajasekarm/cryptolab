# crypto-dashboard

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your end-to-end tests
```
yarn run test:e2e
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



277AA4B4-F405-4F9A-9F85-098E115B4B59
5C704D0D-1963-4C7B-A590-A5A09773E964
31587BAC-9AF3-4CA5-9DB9-B633F518B636


