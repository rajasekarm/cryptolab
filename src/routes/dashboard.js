import Dashboard from "@/views/Dashboard";

const dashboard = [
  {
    path: "/",
    redirect: "/dashboard"
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard
  }
];

export default dashboard;
