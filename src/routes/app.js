import PageNotFound from "@/views/404";

const app = [
  {
    path: "/page-not-found",
    name: "PageNotFound",
    component: PageNotFound
  }
];

export default app;
