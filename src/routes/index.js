import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import app from "./app";
import dashboard from "./dashboard";

const routes = [...app, ...dashboard];

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next({ path: "/page-not-found" });
  } else {
    next();
  }
});

export default router;
