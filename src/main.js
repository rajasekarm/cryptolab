import Vue from "vue";
import vueDebounce from "vue-debounce";

import App from "@/App.vue";
import router from "@/routes";
import store from "@/store";

import Spinner from "@/components/common/Spinner";
Vue.component("Spinner", Spinner);
Vue.use(vueDebounce);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
