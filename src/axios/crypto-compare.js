import axios from "axios";
import constants from "@/constants/key";

const baseURL = "https://min-api.cryptocompare.com";
const axiosInstance = axios.create({ baseURL, timeout: 30000 });

axiosInstance.interceptors.request.use(
  config => {
    const apiKey = {
      api_key: constants.cryptocompare
    };
    config.params = { ...config.params, ...apiKey };
    return config;
  },
  error => Promise.reject(error)
);

export default axiosInstance;
