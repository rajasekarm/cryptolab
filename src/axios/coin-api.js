import axios from "axios";
import constants from "@/constants/key";

const baseURL = "https://rest.coinapi.io";
const axiosInstance = axios.create({ baseURL, timeout: 30000 });

axiosInstance.interceptors.request.use(
  config => {
    config.headers["X-CoinAPI-Key"] = constants.coinapi;
    return config;
  },
  error => Promise.reject(error)
);

export default axiosInstance;
