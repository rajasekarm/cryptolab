import Vue from "vue";
import Vuex from "vuex";

import coin from "./modules/coin/";

Vue.use(Vuex);

function enableNamespacedModule() {
  const modules = { coin };
  const keys = Object.keys(modules);
  keys.forEach(module => {
    modules[module].namespaced = true;
  });
  return modules;
}

const modules = enableNamespacedModule();
export default new Vuex.Store({ modules });
