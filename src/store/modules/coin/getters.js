const getters = {
  isLoading: state => state.isLoading,
  isPriceLoading: state => state.isPriceLoading,
  price: state => state.coinPrice,
  list: state => state.list,
  isHistoryLoading: state => state.isHistoryLoading,
  history: state => state.history,
  historyError: state => state.historyError
};
export default getters;
