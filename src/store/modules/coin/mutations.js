const mutation = {
  COINS_LOADING(state, status) {
    state.isCoinsLoading = status;
  },

  SAVE_COINS(state, data) {
    state.list = [...data];
  },

  COIN_PRICE_LOADING(state, status) {
    state.isPriceLoading = status;
  },

  SAVE_COIN_PRICE(state, price) {
    state.coinPrice = { ...price };
  },

  COIN_HISTORY_LOADING(state, status) {
    state.isHistoryLoading = status;
  },

  SAVE_COIN_HISTORY(state, history) {
    state.history = [...history];
  },

  SAVE_COIN_HISTORY_ERROR(state, status) {
    state.historyError = status;
  }
};
export default mutation;
