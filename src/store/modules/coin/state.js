const state = {
  isCoinsLoading: false,
  list: [],
  isPriceLoading: false,
  coinPrice: {},
  isHistoryLoading: false,
  history: [],
  historyError: false
};
export default state;
