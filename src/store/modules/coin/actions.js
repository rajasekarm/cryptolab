import cryptoCompare from "@/axios/crypto-compare";

const actions = {
  async list({ commit }) {
    try {
      commit("COINS_LOADING", true);
      const result = await cryptoCompare("/data/all/coinlist");
      commit("SAVE_COINS", result.data);
      return result.data;
    } catch (err) {
      return err;
    } finally {
      commit("COINS_LOADING", false);
    }
  },

  async getPrice({ commit }, { coinId }) {
    try {
      commit("COIN_PRICE_LOADING", true);
      const params = { fsyms: coinId, tsyms: "USD" };
      const result = await cryptoCompare("data/pricemultifull", { params });
      commit("SAVE_COIN_PRICE", result.data);
      return result.data;
    } catch (err) {
      return err;
    } finally {
      commit("COIN_PRICE_LOADING", false);
    }
  },

  async getHistoricalData({ commit }, { coinId }) {
    try {
      commit("COIN_HISTORY_LOADING", true);
      commit("SAVE_COIN_HISTORY_ERROR", false);
      const params = { limit: 100, fsym: coinId, tsym: "USD" };
      const result = await cryptoCompare.get(`data/histohour`, { params });
      commit("SAVE_COIN_HISTORY", result.data.Data);
      return result.data.Data;
    } catch (err) {
      commit("SAVE_COIN_HISTORY_ERROR", true);
    } finally {
      commit("COIN_HISTORY_LOADING", false);
    }
  }
};
export default actions;
