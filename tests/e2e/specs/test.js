// https://docs.cypress.io/api/introduction/api.html

describe("Cryptolabs Homepage", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Crypto Labs");
  });
});
