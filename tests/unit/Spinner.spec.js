import { shallowMount } from "@vue/test-utils";
import Spinner from "@/components/common/Spinner.vue";

describe("Spinner.vue", () => {
  it("renders spinner", () => {
    const wrapper = shallowMount(Spinner);
    expect(wrapper.attributes("class")).toMatch("spinner");
  });
});
