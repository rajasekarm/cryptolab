import { shallowMount } from "@vue/test-utils";
import DataList from "@/components/common/DataList.vue";

describe("DataList.vue", () => {
  it("renders empty text", () => {
    const wrapper = shallowMount(DataList, {
      propsData: { items: [], noData: "No coins found" }
    });
    expect(wrapper.html()).toMatch("No coins found");
  });

  it("renders given list", () => {
    const items = [{ asset_id: "BTC", name: "Bitcoin" }];
    const wrapper = shallowMount(DataList, {
      propsData: { items: items, selected: items[0], id: "asset_id" }
    });
    expect(wrapper.text()).toMatch("Bitcoin");
  });
});
